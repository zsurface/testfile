package classfile;

import java.io.*;
import java.lang.String;
import java.util.*;
import classfile.Tuple;

public final class Print{
    public static void fl() {
        String s = Aron.replicate(80, "-");
        System.out.println(s);
    }
    public static void fl(String s) {
        int num = (80 - s.length())/2;
        String str = Aron.replicate(num, "-") + s + Aron.replicate(num, "-");
        System.out.println(str);
    }
    public static void ln(String s) {
        fl(s);
    }
    public static void ln() {
        fl();
    }
    public static <T> void fl(String s, T data) {
        fl(s);
        p(data); 
    }
    public static void p(boolean a) {
        System.out.println(a);
    }
    public static void p(char a) {
        System.out.println(a);
    }
    public static void p(double a) {
        System.out.println(a);
    }
    public static void p(float a) {
        System.out.println(a);
    }
    public static void p(int a) {
        System.out.println(a);
    }
    public static void p(long a) {
        System.out.println(a);
    }
    public static void p(Object a) {
        System.out.println(a);
    }
    public static void p(String a) {
        System.out.println(a);
    }
    public static void p() {
        System.out.println();
    }

    public static <T> void p(T[] arr) {
        if(arr != null) {
            for(T t : arr) {
                System.out.print("["+ t +"]");
            }
            System.out.println();
        }
    }
    public static <A, B> void p(Tuple<A, B> tuple){
        p("(" + tuple.x + "," + tuple.y + ")");
    }

    public static void p(char[] arr) {
        if(arr != null) {
            for(char t : arr) {
                System.out.print("["+ t +"]");
            }
            System.out.println();
        }
    }

    public static void p(int[] arr) {
        if(arr != null) {
            for(int t : arr) {
                System.out.print("["+ t +"]");
            }
            System.out.println();
        }
    }

    public static void p(byte[] arr) {
        if(arr != null) {
            for(int t : arr) {
                System.out.print("["+ t +"]");
            }
            System.out.println();
        }
    }

    public static void pListByte(List<byte[]> list) {
        if(list != null) {
            for(byte[] byteArr : list) {
                p(byteArr);
            }
            System.out.println();
        }
    }

    public static void p(double[] arr) {
        if(arr != null) {
            for(double t : arr) {
                System.out.print("["+ t +"]");
            }
            System.out.println();
        }
    }

    public static void p(long[] arr) {
        if(arr != null) {
            for(long t : arr) {
                System.out.print("["+ t +"]");
            }
            System.out.println();
        }
    }

    public static void p(double[][] arr) {
        if(arr != null) {
            for(int c=0; c<arr.length; c++) {
                for(int r=0; r<arr[0].length; r++) {
                    System.out.print("["+arr[c][r]+"]");
                }
                System.out.println();
            }
        }
        System.out.println();
    }

    public static void p(int[][] arr) {
        if(arr != null) {
            for(int c=0; c<arr.length; c++) {
                for(int r=0; r<arr[0].length; r++) {
                    System.out.print("["+arr[c][r]+"]");
                }
                System.out.println();
            }
        }
        System.out.println();
    }


    public static void p(long[][] arr) {
        if(arr != null) {
            int nrow = arr.length;
            int ncol = arr[0].length; 
            for(int c=0; c<nrow; c++) {
                for(int r=0; r<ncol; r++) {
                    System.out.print("["+arr[c][r]+"]");
                }
                System.out.println();
            }
        }
        System.out.println();
    }

    public static void p(char[][] arr) {
        if(arr != null) {
            for(int c=0; c<arr.length; c++) {
                for(int r=0; r<arr[0].length; r++) {
                    String s = arr[c][r] + "";
                    System.out.print("["+ s +"]");
                }
                System.out.println();
            }
        }
        System.out.println();
    }


    public static void pp(boolean a) {
        System.out.print(a);
    }
    public static void pp(char a) {
        System.out.print(a);
    }
    public static void pp(char[] a) {
        System.out.print(a);
    }
    public static void pp(double a) {
        System.out.print(a);
    }
    public static void pp(float a) {
        System.out.print(a);
    }
    public static void pp(int a) {
        System.out.print(a);
    }
    public static void pp(long a) {
        System.out.print(a);
    }
    public static void pp(Object a) {
        System.out.println(a);
    }
    public static void pp(String a) {
        System.out.print(a);
    }

    public static void pl(boolean a) {
        System.out.println(a);
    }
    public static void pl(char a) {
        System.out.println(a);
    }
    public static void pl(char[] a) {
        System.out.println(a);
    }
    public static void pl(double a) {
        System.out.println(a);
    }
    public static void pl(float a) {
        System.out.println(a);
    }
    public static void pl(int a) {
        System.out.println(a);
    }
    public static void pl(long a) {
        System.out.println(a);
    }
    public static void pl(Object a) {
        System.out.println(a);
    }
    public static void	print(String a) {
        System.out.println(a);
    }
    public static void pp(Collection<?> c){
        for(Object o : c){
            System.out.println(o);
        }
    }

    public static void pb(boolean a) {
        System.out.print("[" + a + "]");
    }
    public static void pb(char a) {
        System.out.print("[" + a + "]");
    }
    public static void pb(char[] a) {
        System.out.print("[" + a + "]");
    }
    public static void pb(double a) {
        System.out.print("[" + a + "]");
    }
    public static void pb(float a) {
        System.out.print("[" + a + "]");
    }
    public static void pb(int a) {
        System.out.print("[" + a + "]");
    }
    public static void pb(long a) {
        System.out.print("[" + a + "]");
    }
    public static void pb(Object a) {
        System.out.print("[" + a + "]");
    }
    public static void pb(String a) {
        System.out.print("[" + a + "]");
    }

    public static <T> void pb(T a, String b) {
        if(b.equals("("))
            System.out.println("(" + a + ")");
        else if (b.equals("<"))
            System.out.println("<" + a + ">");
        else if(b.equals("{"))
            System.out.println("{" + a + "}");
        else if(b.equals(" "))
            System.out.println(" " + a + " ");
        else
            System.out.println("[" + a + "]");
    }

    public static <T> void p(List<T> list) {
        printList(list);
    }

    public static <T> void p(List<T> list, String b) {
        printList(list, b);
    }

    public static <A, B> void p(Map<A, B> map) {
        printMap(map);
    }

    public static <A, B> void p(Map<A, B> map, String s) {
        fl(s);
        printMap(map);
        fl();
    }

    public static <T> void pArr(List<T[]> list) {
        for(T[] arr : list) {
            for(T n : arr){ 
                System.out.print("[" + n + "]");
            }
        }
        System.out.println();
    }
    public static <T> void printList(List<T> list) {
        for(T item : list) {
            p(item);
        }
        System.out.println();
    }

    public static <T> void printList(List<T> list, String b) {
        for(T item : list) {
            Print.pb(item, b);
        }
        System.out.println();
    }

    public static <A, B> void printMap(Map<A, B> map) {
        for(Map.Entry<A, B> e : map.entrySet()){
            p(e.getKey() + " " + e.getValue());
        }
        System.out.println();
    }
    public static <A, B> void printListPair(List<Tuple<A, B>> list){
        for(Tuple<A, B> pa : list){
           p(pa.x + "->" + pa.y); 
        }
    }

    public static <T> void printList2d(List<ArrayList<T>> lists) {
        for(ArrayList<T> list : lists) {
            p(list);
            pl("");
        }
    }


    public static void pbl(boolean a) {
        System.out.println("[" + a + "]");
    }
    public static void pbl(char a) {
        System.out.println("[" + a + "]");
    }
    public static void pbl(char[] a) {
        System.out.println("[" + a + "]");
    }
    public static void pbl(double a) {
        System.out.println("[" + a + "]");
    }
    public static void pbl(float a) {
        System.out.println("[" + a + "]");
    }
    public static void pbl(int a) {
        System.out.println("[" + a + "]");
    }
    public static void pbl(long a) {
        System.out.println("[" + a + "]");
    }
    public static void pbl(Object a) {
        System.out.println("[" + a + "]");
    }
    public static void pbl(String a) {
        System.out.println("[" + a + "]");
    }
    public static void pb(String a, String b) {
        if(b.equals("("))
            System.out.print(" (" + a + ") ");
        else if (b.equals("<"))
            System.out.print(" <" + a + "> ");
        else if(b.equals("{"))
            System.out.print(" {" + a + "} ");
        else
            System.out.print(" [" + a + "] ");
    }
    public static void pbl(String a, String b) {
        if(b.equals("("))
            System.out.println("(" + a + ")");
        else if (b.equals("<"))
            System.out.println("<" + a + ">");
        else if(b.equals("{"))
            System.out.println("{" + a + "}");
        else
            System.out.println("[" + a + "]");
    }
    public static <T> void pr(List<T> ls){
        for(T t : ls){
            System.out.print(t + " ");
        }
    }

    public static void printCharacter2D(Character[][] arr){
        for(int h=0; h<arr.length; h++){
            p(arr[h]);
        }
    }
    public static void printChar2D(char[][] arr){
        for(int h=0; h<arr.length; h++){
            p(arr[h]);
        }
    }
}


