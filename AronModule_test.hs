-- | Sun Oct 21 00:00:05 2018 
-- | outer product = col ⊕ row

--------------------------------------------------------------------------------- 
outer::(Num a)=>[a]->[a]->[[a]]
outer s1 s2 = map(\x1 -> map(\x2 -> x1*x2) s2) s1

outerStr::(a -> a-> a)->[[a]]->[[a]]->[[a]]
outerStr f v r = [ map(\rr -> f (head v') rr ) r' | v' <- v, r' <- r]


vcol::[[a]] -> Int -> [[a]]  
vcol m n = tran $ vrow (tran m) n

vrow::[[a]] -> Int -> [[a]]
vrow m n = drop (n - 1) $ take n m

-- sum matrix
-- matSum::(Num a)=>[[a]]->[[a]]->[[a]]
-- matSum m1 m2 = zipWith(\x y -> zipWith(\x1 y1 -> x1+y1) x y) m1 m2 

{-| 
    === zipWith in two dimensions, zipWith matrix
    @
    zipWith2(Num a) => (a -> a -> a) ->[[a]] ->[[a]] -> [[a]]
    zipWith2 f a b = [ zipWith f ra rb | (ra, rb) <- zip a b]

    zipWith2::(Num a)=>(a ->a ->a)->[[a]]->[[a]]->[[a]]
    zipWith2 f m1 m2 = zipWith(\x y -> zipWith(\x1 y1 -> f x1 y1) x y) m1 m2 
    @
-} 
zipWith2::(Num a)=>(a ->a ->a)->[[a]]->[[a]]->[[a]]
zipWith2 f m1 m2 = zipWith(\x y -> zipWith(\x1 y1 -> f x1 y1) x y) m1 m2 



data XNode = XNode (M.HashMap Char XNode)  Bool deriving(Eq, Show)
{-| 
    === insert operation for Tries data structure

    >let xn = insertTries "a" (XNode M.empty False)
    >let xxn = insertTries"ab" xn
    >pp $ "containsTries=" <<< (containsTries "a" xxn == True)
    >pp $ "containsTries=" <<< (containsTries "ab" xxn == True)

    1. If String is empty return XNode is the end of a word
    2. If x points to Nothing(x is not on the Tries), then recur to cx 
    3. If x points to an XNode, then recur to cx
-} 
insertTries::String -> XNode -> XNode
insertTries [] (XNode m _) = XNode m True  -- it is a word
insertTries (x:cx) (XNode m b) = case xn of 
                                 Nothing  -> XNode (M.insert x (insertTries cx (XNode M.empty False)) m) b 
                                 Just xn' -> XNode (M.insert x (insertTries cx xn') m) b 
                where
                    xn = M.lookup x m 

{-| 
    === Insert list of strings to Tries, see 'insertTries'
-} 
insertTriesList::[String] -> XNode -> XNode
insertTriesList [] n = n
insertTriesList (x:cx) n = insertTriesList cx (insertTries x n)

{-| 
    === contain operation for Tries data structure

    >let xn = insertTries "a" (XNode M.empty False)
    >let xxn = insertTries"ab" xn
    >pp $ "containsTries=" <<< (containsTries "a" xxn == True)
    >pp $ "containsTries=" <<< (containsTries "ab" xxn == True)
-} 
containsTries::String -> XNode -> Bool
containsTries [] (XNode m b) = True 
containsTries (x:cx) (XNode m b) = case xn of 
                                   Nothing -> False
                                   Just xn' -> containsTries cx xn'
                where
                    xn = M.lookup x m


{-| 
    === Most functions are related to Binary Search Tree
    * Thu Nov  8 21:24:37 2018 
-} 
data Tree a = Empty 
            | Node a (Tree a) (Tree a) deriving (Show, Eq)

insertNode::(Ord a)=>Tree a -> a -> Tree a
insertNode Empty a = Node a Empty Empty 
insertNode (Node a left right) b = if b < a then (Node a (insertNode left b) right) else (Node a left (insertNode right b))

{-| 
    === Insert a list of element into a 'Tree'
-} 
insertFromList::(Ord a)=>Tree a ->[a]->Tree a
insertFromList Empty [] = Empty
insertFromList (Node a l r) [] = Node a l r
insertFromList Empty (x:xs) = insertFromList (insertNode Empty x) xs 
insertFromList (Node a l r) (x:xs) = insertFromList (insertNode (Node a l r) x) xs


{-| 
    === Inorder insection
-} 
inorder::Tree a->[a]
inorder Empty = []
inorder (Node a l r) = (inorder l) ++ [a] ++ (inorder r) 

{-| 
    === Maximum number of levels of a 'Tree' 
-} 
maxlen::Tree a->Integer
maxlen Empty = 0 
maxlen (Node a l r) = 1 + max (maxlen l) (maxlen r)

{-| 
    === Better head 
    * TODO: Use better error message
-} 
head'::[a]->a
head' cx = if length cx > 0 then head cx else error "list is empty"

{-| 
    === check whether a Tree is Binary tree

    == defintion of BST

    * Null is BST
    * Left subtree is BST
    * Right subtree is BST
    * minimum of left subtree is less than parent node
    * maximum of right subtree is greater than parent node
-} 
isBST::(Ord a)=>Tree a -> Bool
isBST Empty = True
isBST (Node a l r) =   isBST l 
                    && isBST r 
                    && (if l /= Empty then max l < a else True) 
                    && (if r /= Empty then min r > a else True)   
            where
                min::(Ord a)=>Tree a -> a 
                min Empty                = error "min error"
                min (Node a Empty Empty) = a
                min (Node a l _)         = min l
                max::(Ord a)=>Tree a -> a 
                max Empty                = error "max error"
                max (Node a Empty Empty) = a
                max (Node a _ r)         = max r

{-| 
    === Binary tree insection
-} 
binsert::Tree Integer->Tree Integer->Tree Integer 
binsert Empty (Node a Empty Empty) = (Node a Empty Empty)
binsert (Node a l r) (Node b Empty Empty) = if b < a 
                                            then Node a (binsert l (Node b Empty Empty)) r 
                                            else Node a l (binsert r (Node b Empty Empty))   


sym::Tree a ->Bool
sym Empty = True
sym (Node a Empty Empty) = True
sym (Node a l Empty) = False 
sym (Node a Empty r) = False 
sym (Node a l r) = sym l && sym r 
                
{-| 
    === Lease common ancestor

    * assume two nodes are in the tree 
    * if two nodes are in the same path then the top node will be LCA 
-} 
lca::(Eq a)=>Tree a -> a -> a -> Maybe a 
lca Empty _ _ = Nothing 
lca (Node a l r) x y = if a == x || a == y then Just a else 
                            let left = lca l x y; right = lca r x y
                                in if left /= Nothing && right /= Nothing then Just a
                                    else if left == Nothing then right else left 


           
{-| 
    === Build binary tree from preorder and inorder
-} 
buildTree::[Char]->[Char]->Tree Char 
buildTree _ [] = Empty
buildTree [] _ = Empty
buildTree preorder inorder = Node h  (buildTree leftPre leftIn) (buildTree rightPre  rightIn) 
                            where
                                h = head preorder
                                leftIn  = filter(\x->x < h) inorder
                                rightIn = filter(\x->x > h) inorder
                                leftPre = take (length rightIn) $ tail preorder 
                                rightPre = subList preorder (length leftIn) $ length preorder
                                -- ['a', 'b', 'c', 'd']
                                -- subList s 1 2 => 'b'
                                subList::[a]->Int->Int->[a]
                                subList [] _ _ = [] 
                                subList _  m n | m >= n = []
                                subList (x:xs) m n 
                                                 | m > 0 = subList xs (m-1) (n-1) 
                                                 | m == 0 = take (n) (x:xs) 



{-| 
    === Find all anagrams from a list of strings

    >anagram "dog" ["god", "cat", "ogd"] 
    >["god", "ogd"]
-} 
anagram::String->[String] -> [String]
anagram s cx = let mlist = M.lookup (sort s) map in if mlist == Nothing then [] else fromJust mlist 
    where
        map = insertMap M.empty cx 
        sort = L.sort
        insertMap::M.HashMap String [String] -> [String] -> M.HashMap String [String]
        insertMap m [] = m
        insertMap m (x:cx) = insertMap (insertStr m x) cx
                where 
                    insertStr::M.HashMap String [String] -> String -> M.HashMap String [String]
                    insertStr m s = nmap 
                        where
                            nmap = let s' = sort s; v = M.lookup s' m 
                                    in if v == Nothing then M.insert s' [s] m 
                                                       else let k = fromJust v in M.insert s' (s:k) m 


redisExtractAronModule::[String] -> [([String], Integer, [String])]
redisExtractAronModule [] = []
redisExtractAronModule cx = pMap 
    where
       -- list = filter(\e -> matchTest (makeRegex ("(^[a-zA-Z0-9_]+)[[:space:]]*::")::TD.Regex) e) cx 
       -- list = filter(\e -> matchTest (makeRegex ("(^[a-zA-Z0-9_]+)[[:space:]]*::")::TD.Regex) e) cx 
       -- lss = map(\e -> (matchAllText (makeRegex ("(^[a-zA-Z0-9_]+)[[:space:]]*::")::TD.Regex) $ e, e)) list 
       list = filter(\e -> matchTest (mkRegex ("(^[a-zA-Z0-9_]+)[[:space:]]*::")) e) cx 
       lss = map(\e -> (matchAllText (mkRegex ("(^[a-zA-Z0-9_]+)[[:space:]]*::")) $ e, e)) list 
       ln = map(\x -> (DR.elems $ (fst x) !! 0, snd x)) lss
       lns = map(\x -> (fst $ head $ tail $ fst x, snd x)) ln
       rMap = zipWith(\n x -> (prefix $ fst x, n, [snd x])) [30000..] lns
       --
       -- => [([AronModule.k0, AronModule.k1..], Integer, [String])]
       pMap = map (\ls -> (map(\x -> package ++ x) $ t1 ls, t2 ls, t3 ls)) rMap
       package = "AronModule." -- append to each keys

{-| 

    KEY: Parse Java method name, extract method name and form a list
        [([String], Integer, [String])]


    File format:
    jname = "/Users/cat/myfile/bitbucket/javalib/Aron.java"

    The list can be used in Redis Server

    >["line1", "line2"] -> [([k0, k1], 1, ["line1"])]
-} 
redisExtractJavaMethod::[String] -> [([String], Integer, [String])]
redisExtractJavaMethod [] = []
redisExtractJavaMethod cx = pMap 
    where
        list = filter(\e -> matchTest (mkRegex "public static") e) cx 
        ls = zipWith(\n x -> (n, x)) [10000..] list
        m = map(\x -> (let may = matchRegex (mkRegex regexJavaMethod ) (snd x)
                       in case may of
                                Just e -> head e 
                                _      -> [] 
                       , fst x, trim $ snd x)
               ) ls   
        lss = map(\x -> (takeWhile(\e -> isLetter e || isDigit e) (t1 x), t2 x, t3 x)) m 
        --
        -- => [([String], Integer, [String])]
        rMap = map(\x ->let la = splitStrChar "[[:space:]]" $ t3 x 
                            lb = init $ foldr(\a b -> a ++ " " ++ b) [] $ drop 2 la 
                        in (prefix $ t1 x, t2 x, [lb])) lss
        --
        -- => [([Aron.k0, Aron.k1..], Integer, [String])]
        pMap = map (\ls -> (map(\x -> package ++ x) $ t1 ls, t2 ls, t3 ls)) rMap
        regexJavaMethod = "([a-zA-Z0-9_]+[[:space:]]*\\([^)]*\\))"
        package = "Aron." -- append to each keys

{-| 

    KEY: Parse Java method name, extract method name and form a list
        [([String], Integer, [String])]


    File format:
    jname = "/Users/cat/myfile/bitbucket/javalib/Aron.java"

    The list can be used in Redis Server

    >["line1", "line2"] -> [([k0, k1], 1, ["line1"])]
-} 
redisExtractJavaMethodWithPackage::String -> [String] -> [([String], Integer, [String])]
redisExtractJavaMethodWithPackage _  [] = []
redisExtractJavaMethodWithPackage package cx = pMap 
    where
        list = filter(\e -> matchTest (mkRegex "public static") e) cx 
        ls = zipWith(\n x -> (n, x)) [10000..] list
        m = map(\x -> (let may = matchRegex (mkRegex regexJavaMethod ) (snd x)
                       in case may of
                                Just e -> head e 
                                _      -> [] 
                       , fst x, trim $ snd x)
               ) ls   
        lss = map(\x -> (takeWhile(\e -> isLetter e || isDigit e) (t1 x), t2 x, t3 x)) m 
        --
        -- => [([String], Integer, [String])]
        rMap = map(\x ->let la = splitStrChar "[[:space:]]" $ t3 x 
                            lb = init $ foldr(\a b -> a ++ " " ++ b) [] $ drop 2 la 
                        in (prefix $ t1 x, t2 x, [lb])) lss
        --
        -- => [([Aron.k0, Aron.k1..], Integer, [String])]
        pMap = map (\ls -> (map(\x -> package ++ x) $ t1 ls, t2 ls, t3 ls)) rMap
        regexJavaMethod = "([a-zA-Z0-9_]+[[:space:]]*\\([^)]*\\))"
        -- package = "Aron." -- append to each keys

{-|                                                            
   Html textarea                                               
   textArea row col string                                     
   textArea 4 5 "dog"                                          
-}                                                             
textArea::Integer -> Integer -> String-> String                
textArea r c s = topen ++ row ++ col ++ bclose ++ s ++ tclose  
    where                                                      
        topen = "<textarea"                                    
        bclose = ">"                                           
        row = sp ++ "rows=\"" ++ (show r) ++ "\""              
        col = sp ++ "cols=\"" ++ (show c) ++ "\""              
        tclose = "</textarea>"                                 
        sp = " "                                               

{-| 
    === Find the name of OS from __environment_variable__ __OSTYPE__

    * Some OS might not set the environment variable name: __OSTYPE__
    * OSTYPE might be set manually in file such as __.profile__
    * Currently it supports MacOS and FreeBSD
    * MacOS = "darwin"
    * FreeBSD = "freebsd"
-} 
getOS::IO String
getOS = do
        osv <- getEnv osType 
        let os = if | containStr macOS osv   -> macOS 
                    | containStr freeBSD osv -> freeBSD 
                    | otherwise              -> error unknown
        return os
    where
        macOS = "darwin"
        freeBSD = "freebsd"
        osType = "OSTYPE"
        unknown = "unknown"

{-| 
    === print matrix, show matrix
-} 
pa::(Show a)=>[[a]]->IO()
pa x = mapM_ print x

pa1::(Show a)=>[a]->IO()
pa1 x = mapM_ print x

{-| 
    === Partition list to n chunks

    __Note__: print partList n [] will cause error since print needs concrete type 

    split list to n blocks

    >partList 2 [1, 2, 3, 4, 5] 
    >[[1,2], [3, 4],[5]]
    >
    >partList 2 [1, 2, 3, 4, 5, 6] 
    >[[1,2], [3, 4],[5, 6]]
-} 
partList::Int->[a]->[[a]]
partList _ [] = []
partList n xs = (take n xs) : (partList n $ drop n xs)

partList2 ::Int->[a]->[[a]]
partList2 _ [] = []
partList2 n xs = (take n xs) : (partList n $ drop 1 xs)

